import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class S2 implements ActionListener {
    int number1;
    int number2;
    JFrame frame;
    JButton button;
    JTextField textField1;
    JTextField textField2;
    JTextField textField3;
    JPanel panel;

    public S2() {
        frame = new JFrame();

        textField1 = new JTextField();
        textField1.setBounds(60, 60, 300, 40);

        textField2 = new JTextField();
        textField2.setBounds(60, 60, 300, 40);

        textField3 = new JTextField();
        textField3.setBounds(60, 60, 300, 40);
        textField3.setEditable(false);

        button = new JButton("+");
        button.addActionListener(this);


        panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(80, 80, 80, 80));
        panel.setLayout(new GridLayout(0, 1));
        panel.add(textField1);
        panel.add(textField2);
        panel.add(textField3);
        panel.add(button);

        frame.add(panel, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("add numbers");
        frame.pack();
        frame.setVisible(true);
    }


    public static void main(String[] args) {
        new S2();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        number1 = Integer.parseInt(textField1.getText());
        number2 = Integer.parseInt(textField2.getText());
        textField3.setText(String.valueOf(number1 + number2));


    }
}